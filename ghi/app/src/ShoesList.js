import { Link } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import ShoesListItem from './ShoesListItem'


function ShoesList(props) {
    const [shoes, setShoes] = useState([]);

    const getShoes = async function() {
        const url = "http://localhost:8080/api/shoes/"
        const response = await fetch(url);
        const shoesObj = await response.json();
        const shoes = shoesObj.shoes
        setShoes(shoes);
        console.log(shoes)
        // console.log("is it an array?", Array.isArray(shoes), shoes)
    }

    useEffect(() => {
        getShoes();
    }, [])

    const deleteShoe = async function(id, name){
        console.log(`attempting to delete ${name}.`)
            const url = `http://localhost:8080/api/shoes/${id}/`
            const res = await fetch(url, {method: 'delete'})
            if (res.ok){
                const newShoesList = shoes.filter(shoe => shoe.id !== id);
                setShoes(newShoesList)
            }}

    return (
        <div>
            <h1>SHOES</h1>
            <Link to="/shoes/create/">Log a Shoe!</Link>
            <ul>
                <div className="row">
                {shoes.map(shoe => <ShoesListItem key={shoe.id} shoe={shoe} deleteShoe={deleteShoe}/>)}
                </div>
            </ul>
        </div>
    );
}


export default ShoesList;
