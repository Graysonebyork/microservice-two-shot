import React from "react";

function ShoesListItem({ shoe, deleteShoe }){

    return(
        <div className="card w-25 m-2">
            <img src={shoe.pictureUrl} className="card-img-top" />
            <div className="card-body">
                <h6 className="card-title">{shoe.manufacturer.name} - {shoe.model_name}</h6>
                <div>Color: {shoe.color}</div>
                <div>Location: {shoe.bin.closet_name} - {shoe.bin.bin_number}</div>
                <div className="btn btn-danger w-100 mt-3" onClick={() => deleteShoe(shoe.id, shoe.model_name)} >DELETE</div>
            </div>
        </div>
    )
}

export default ShoesListItem;
