import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ShoesList from './ShoesList';
import CreateShoe from './CreateShoe'
import Nav from './Nav';
import HatsList from "./HatsList";
import CreateHat from "./CreateHat";


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats/" element={<HatsList />} />
          <Route path="shoes/" element={<ShoesList />} />
          <Route path="shoes/create/" element={<CreateShoe />} />
        </Routes>
      </div>
      <div>
      </div>
    </BrowserRouter>
  );
}

export default App;
