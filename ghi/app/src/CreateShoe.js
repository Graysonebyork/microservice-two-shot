import React, { useEffect, useState } from 'react';

function CreateShoe() {

    const [modelName, setModelName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [manufacturers, setManufacturers] = useState([]);
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins]= useState([]);

    const fetchData = async () => {
        const manufacturerUrl = 'http://localhost:8080/api/manufacturers/';
        const binsUrl = "http://localhost:8080/api/bins/";
        const manufacturerResponse = await fetch(manufacturerUrl);
        const binsResponse = await fetch(binsUrl);
        if (manufacturerResponse.ok) {
            const manufacturerData = await manufacturerResponse.json();
            setManufacturers(manufacturerData.manufacturer)
            console.log("manufData:", manufacturerData.manufacturer)
        }
        if (binsResponse.ok) {
            const binsData = await binsResponse.json();
            console.log("try1:", binsData, ":try2:", binsData.bin)
            setBins(binsData.bin)
            console.log("binsdata:", bins)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        console.log("event:", event)
        event.preventDefault();
        const data = {};
        data.model_name = modelName;
        data.color = color;
        data.manufacturer = manufacturer;
        data.pictureUrl = picture;
        data.bin = bin;
        console.log("this is the data I made:", data)

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const shoesResponse = await fetch(shoeUrl, fetchOptions);
        if (shoesResponse.ok) {
            setManufacturer('');
            setColor('');
            setModelName('');
            setPicture('');
            setBin('');
        }
    }

    const handleChangeModelName = (event) => {
        const value = event.target.value;
        setModelName(value);
    }
    const handleChangeManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }
    const handleChangeColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handleChangePicture = (event) => {
        const value = event.target.value;
        setPicture(value);
    }
    const handleChangeBin = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    return (
        <div >
            <form onSubmit={handleSubmit} id="create-form">
                <h1>Gimme those shoes</h1>
                <div>bin
                    <select onChange={handleChangeBin} name="bin" id="bin" className={'form-select'} value={bin}>
                        <option value="">Where we going?</option>
                        {bins.map(bin => {
                            return (
                                <option key={bin.import_href} value={bin.import_href}>{bin.closet_name}-{bin.bin_number}</option>
                            )
                        })}
                    </select>
                </div>
                <div>
                    <label htmlFor="modelName">Shoe name</label>
                    <input onChange={handleChangeModelName} required placeholder="shoe name" value={modelName} type="text" name="modelName" id="modelName" className="form-control"/>
                </div>
                <div>
                    <label htmlFor="modelName">Shoe color</label>
                    <input onChange={handleChangeColor} required placeholder="shoe color" value={color} type="text" name="color" id="color" className="form-control"/>
                </div>
                <div>
                    <label htmlFor="modelName">Picture url</label>
                    <input onChange={handleChangePicture} required placeholder="picture" value={picture} type="text" name="picture" id="picture" className="form-control"/>
                </div>
                <div>manufacturer
                    <select onChange={handleChangeManufacturer} name="manufacturer" id="manufacturer" className={'form-select'} value={manufacturer}>
                        <option value="">Who was makin it?</option>
                        {manufacturers.map(manufacturer => {
                            return (
                                <option key={manufacturer.name} value={manufacturer.name}>{manufacturer.name}</option>
                            )
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    );
}

export default CreateShoe;
