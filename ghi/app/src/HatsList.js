import React, { useState, useEffect } from 'react';
import HatsListItem from './HatsListItem'


function HatsList(props) {
    const [hats, setHats] = useState([]);

    const getHats = async function() {
        const url = "http://localhost:8090/api/hats/"
        const response = await fetch(url);
        const hatsObj = await response.json();
        const hats = hatsObj.hats
        console.log("grayson's method", hats)
        setHats(hats);
        console.log("is it an array?", Array.isArray(hats), hats)

    }




    useEffect(() => {
        getHats();
    }, [])

    const deleteHat = async function(id,name){
        console.log(`attempting to delete ${name}.`)

        const url = `http://localhost:8090/api/hats/${id}/`

        const res = await fetch(url, {method: "delete"})

        if (res.ok){

            const newHatsList = hats.filter(hat => hat.id !== id);
            setHats(newHatsList)
        }



    }




    return (
        <div>
            <h1>Hats</h1>
            {/* <link to="/hats/create/">Log a Hat!</link> */}

            <ul>
                <div className="row">
                {hats.map(hat =>  <HatsListItem key={hat.id} hat={hat} deleteHat={deleteHat}/>)}
                </div>
            </ul>
        </div>
    );


}

export default HatsList;
