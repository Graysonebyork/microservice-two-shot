import React from "react";


function HatsListItem({ hat, deleteHat }){

    return(

        <div className="card w-25 m-2">
            <img src= {hat.picurl} className="card-img-top" />
            <div className="card-body">
                <h6 className="card-title">{hat.model_name}</h6>
                <div>Fabric: {hat.fabric}</div>
                <div>Style: {hat.style}</div>
                <div>Color: {hat.color}</div>
                <div>Location: {hat.location.closet_name}</div>
                <div>id: {hat.id}</div>
                <div className="btn btn-danger w-100 mt-3" onClick={() => deleteHat(hat.id, hat.model_name)} >DELETE</div>
            </div>

        </div>
    )
}

export default HatsListItem;
