from django.shortcuts import render
from .models import Hats, LocationsVO
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

class LocationsVOEncoder(ModelEncoder):
    model = LocationsVO
    properties = [
        "closet_name",
        "import_href",
    ]

class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "style_name",
        "fabric",
        "color",
        "picurl",
        "location",
        "pub_date",
    ]
    encoders = {"location": LocationsVOEncoder()}

# class HatsDetailEncoder(ModelEncoder):


def api_show_hats(request):

    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsDetailEncoder,

        )
    elif request.method == "POST":
            content = json.loads(request.body)

            try:
                 location = LocationsVO.objects.get(import_href=content["location"])
                 content["location"] = location
            except LocationsVO.DoesNotExist:
                return JsonResponse(
                     {"message": "Invalid location reference, hehe"},
                      status=400
                )


            hats = Hats.objects.create(**content)
            return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False
            )

def api_list_hats(request, pk):
    if request.method == "GET":
        hat = Hats.objects.get(id=pk)

        return JsonResponse(
            {"hat": hat},
            encoder = HatsDetailEncoder,
            safe=False

            )
    elif request.method == "DELETE":
        Hats.objects.filter(id=pk).delete()

        return JsonResponse(
             {"message": "Hat successfully deleted"},
        )
