from django.db import models

class LocationsVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.closet_name

class Hats(models.Model):
    style_name = models.CharField(max_length=200)
    fabric = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picurl = models.URLField(max_length=200)
    pub_date = models.DateTimeField('date published')

    location = models.ForeignKey(
        LocationsVO,
        related_name="location",
        on_delete=models.PROTECT,
        null=True
    )

    def __str__(self):
        return self.style_name
