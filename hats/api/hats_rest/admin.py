from django.contrib import admin
from django.conf import settings
from .models import Hats, LocationsVO

@admin.register(Hats)
class HatsAdmin(admin.ModelAdmin):
    list_display = ["style_name", "fabric", "color", "picurl", "pub_date"]

@admin.register(LocationsVO)
class LocationsVOAdmin(admin.ModelAdmin):
    list_display = ["closet_name", "section_number", "shelf_number", "import_href"]

# Register your models here.
