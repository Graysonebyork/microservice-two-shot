# Wardrobify

Team:

* Person 1 - Which microservice?
    Grayson: shoes
* Person 2 - Which microservice?
    Elliott: hats

## Design

I am planning on getting the backend sorted first and testing it with insomnia. After that, I will implement the front end and hopefully have time to do some css/bootstrap styling.

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
