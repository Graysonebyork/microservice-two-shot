from django.db import models
from django.conf import settings


class BinsVO(models.Model):
    #id = the pk in the database
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.closet_name

class Manufacturer(models.Model):
    name = models.CharField(max_length=200)
    country = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Shoes(models.Model):
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    pictureUrl = models.URLField(max_length=200)

    manufacturer = models.ForeignKey(
        Manufacturer,
        related_name="shoes",
        on_delete=models.PROTECT,
        null=True
    )
    bin = models.ForeignKey(
        BinsVO,
        related_name="+",
        on_delete=models.PROTECT,
        null=True
    )

    def __str__(self):
        return self.model_name


# Create your models here.
