from django.contrib import admin
from .models import Shoes, Manufacturer, BinsVO

@admin.register(Shoes)
class ShoesAdmin(admin.ModelAdmin):
    list_display = ["model_name", "color", "pictureUrl", "manufacturer", "bin"]

@admin.register(Manufacturer)
class ManufacturerAdmin(admin.ModelAdmin):
    list_display = ["name", "country"]

@admin.register(BinsVO)
class BinsVOAdmin(admin.ModelAdmin):
    list_display = ["closet_name", "bin_number", "bin_size", "import_href"]

# Register your models here.
