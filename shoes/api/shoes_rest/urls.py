from django.urls import path
from .views import api_list_shoes, api_show_manufacturer, api_show_shoes, api_show_bins

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_show_shoes"),
    path("manufacturers/", api_show_manufacturer, name="api_show_manufacturer"),
    path("shoes/<int:pk>/", api_show_shoes, name="api_show_shoes"),
    path("bins/", api_show_bins, name='api_show_bins')
]
