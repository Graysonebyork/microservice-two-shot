from django.shortcuts import render
from common.json import ModelEncoder
from .models import BinsVO, Manufacturer, Shoes
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json


class ManufacturerEncoder(ModelEncoder):
    model = Manufacturer
    properties = [
        "id",
        "name",
        "country"
    ]

class BinsVOEncoder(ModelEncoder):
    model = BinsVO
    properties = [
        "closet_name",
        "bin_size",
        "bin_number",
        "import_href"
    ]

class BinsVOListEncoder(ModelEncoder):
    model = BinsVO
    properties = [
        "closet_name",
        "bin_number",
    ]

class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "id",
        "model_name",
        "color",
        "pictureUrl",
        "bin",
        "manufacturer"
    ]
    encoders = {
        "bin": BinsVOEncoder(),
        "manufacturer": ManufacturerEncoder()
    }

class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "id",
        "model_name",
        "bin",
        "manufacturer",
        "pictureUrl",
        "color"
    ]
    encoders = {
        "bin": BinsVOListEncoder(),
        "manufacturer": ManufacturerEncoder()
    }


def api_list_shoes(request):

     if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
        )
     elif request.method == "POST":
        content = json.loads(request.body)

        try:
            bin = BinsVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
        except BinsVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin reference, you goose"},
                status=400
            )

        try:
            manufacturer = Manufacturer.objects.get(name=content["manufacturer"])
            content["manufacturer"] = manufacturer
        except Manufacturer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid manufacturer name, you goose"},
                status=400
            )

        shoes = Shoes.objects.create(**content)
        return JsonResponse(
        shoes,
        encoder=ShoesDetailEncoder,
        safe=False
        )

def api_show_shoes(request, pk):
    if request.method == "GET":
        shoe = Shoes.objects.get(id=pk)

        return JsonResponse(
            {"shoe": shoe},
            encoder = ShoesDetailEncoder,
            safe=False
        )

    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


def api_show_manufacturer(request):

    if request.method == "GET":
        manufacturer = Manufacturer.objects.all()
        print(manufacturer)
        return JsonResponse(
            {"manufacturer": manufacturer},
            encoder=ManufacturerEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        manufacturer = Manufacturer.objects.create(**content)
        return JsonResponse(
            manufacturer,
            encoder=ManufacturerEncoder,
            safe=False,
        )

def api_show_bins(request):

    if request.method == "GET":
        bin = BinsVO.objects.all()
        print(bin)
        return JsonResponse(
            {"bin": bin},
            encoder=BinsVOEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        bin = BinsVO.objects.create(**content)
        return JsonResponse(
            BinsVO,
            encoder=BinsVOEncoder,
            safe=False,
        )
